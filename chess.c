/*Douglas Kramer
douglak
Lab2
1021-001
Nick Glyder
*/

#include <stdio.h>
#include <stdlib.h>

void print_board(char board[8][8]);
//int in_jeopardy(int, int, char[8][8]);

int main(int argc, char *argv[])
{
	//declaring a file point to open the text file to read it
	FILE* input = fopen(argv[1], "r");

	char board[8][8];
	int i, j;

	for(i = 0; i < 8; i++)
  {
  	for(j = 0; j < 8; j++)
    {
	  	fscanf(input, "%c", &board[i][j]);
	  }
	}

	//prints the playing board
	print_board(board);

	/*tells the user if the board is vaild or not valid, if not valid it
	  tells the user where it is not valid*/
	//in_jeopardy(i, j, board[8][8])

	return 0;
}

void print_board(char board[8][8])
{
	int i, j;

	 /*for loop to print the rows of the playing board for 'q' and
          then stores the location of the q*/
  for(i = 0; i < 8; i++)
  {
    //for loop to print the columns
    for(j = 0; j < 8; j++)
    {
      printf("%c", board[i][j]);
    }
	printf("\n");
	}
}

/*int in_jeopardy(i, j, board[8][8])
{
	int k;

	return 0;
}*/
